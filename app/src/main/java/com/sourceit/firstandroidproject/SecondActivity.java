package com.sourceit.firstandroidproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        textView=(TextView) findViewById(R.id.textView);
        if (getIntent().hasExtra(MainActivity.LABEL))
        {
            textView.setText(getIntent().getStringExtra(MainActivity.LABEL));
        }
    }


}
