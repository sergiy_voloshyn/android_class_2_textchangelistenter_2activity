package com.sourceit.firstandroidproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    TextView label;
   public static  String LABEL;

    EditText inputText1;
    EditText inputText2;

    Button action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        label = (TextView) findViewById(R.id.some_text);

        label.setText(savedInstanceState==null?"":
        savedInstanceState.getString(LABEL));


        inputText1 = (EditText) findViewById(R.id.editText1);
        inputText2 = (EditText) findViewById(R.id.editText2);

        inputText1.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable editable) {

                showInformation();
            }
        });


        inputText2.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable editable) {

                showInformation();
            }
        });







        action = (Button) findViewById(R.id.button2);


        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    String newLine = inputText1.getText().toString();
             //   action.setText(newLine);
             //   Toast.makeText(MainActivity.this, newLine, Toast.LENGTH_SHORT).show();

                Intent intent =new Intent(MainActivity.this,SecondActivity.class);

                intent.putExtra(LABEL, label.getText().toString());
                startActivity(intent);
            }
        });





        //  helloword.setText(R.string.helloString);
        //   helloword.setTextColor(Color.BLUE);

    }

    private void showInformation() {
        label.setText(inputText1.getText().toString() + inputText2.getText().toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LABEL,label.toString());
    }
}
