package com.sourceit.firstandroidproject;

import android.text.TextWatcher;

/**
 * Created by Student on 23.12.2017.
 */

public abstract class OnTextChangeListener implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }
}
